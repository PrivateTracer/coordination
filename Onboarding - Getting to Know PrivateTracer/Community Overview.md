# Who are the people behind PrivateTracer?
We are not a company doing this for profit, but a consortium of several parties working on a potential responsible solution for the COVID-19 crisis.

We have several subgroups in our community, namely the foundation, group of developers and the collaboration partners.

# Foundation
The foundation was set up to act as the entity with which a government can interact, allowing for participation in formal processes.

# Developer Roles
The following are a subset of the roles on the project, and the reader is asked to recognise that this is both a changing list (both of people and who does what). It is intended to provide a first pass orientation for those newly engaging in the project:
* Initiator & firefigter: Salim Hadri
* Tech Design Focal Point & Delivery Coordinator: Neil Smyth
* Use cases: Rene Honig
* On-boarding & Community: Yvo Hunink
* Android Client: Frank Bouwens 
* Proximity Detection: Kaj Oudshoorn
* End-end integration: Michiel Hegemens 
* DP-3T Protocol: Jelle Licht
* Cloud Services: Steve Kellaway
* iOS Client: Dirk Groten
* UX: Mesbah Sabur
* Doctor Authorisation: Bart van Riessen
* Privacy & Security Coordination: Rene Honig
* Security Threat Analysis: Aleksander Okonski
* Expert advisor: Dirk-Willem van Gulik

And sincere apologies to the many others that have contributed but whose name I have not put up - felt I had to keep the list short.
# Collaboration Partners
Our multi-disciplinary collaboration exists of public, private, academic and non-profit organisations. You can find the [overview of partners on our website](https://www.privatetracer.org/partners).