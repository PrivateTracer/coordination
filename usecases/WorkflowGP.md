# **Workflow General Practitioner within the Netherlands**

The GP is responsible for the primary care of the citizens of the Netherlands. The Municipal Health Services (Known as GGD) have a big role when it comes to prevention of disease. Both GGD and GP have a crucial role in giving care to (suspected) COVID-19 citizens/patients and to prevent further spread.

**Hoofdbehandelaar (Main Practitioner)**

*Hoofdbehandelaarschap* defines which caregiver is the responsible party when it comes to care (outcomes). The GP is the *hoofdbehandelaar* of all citizens and acts as a gatekeeper for citizens needing care. However, this changes when a citizen lives in a home care facility and there is a home care physician (*verpleeghuisarts*) connected to this facility. Furthermore, when you receive care due to chronic illness or due to any disease needing specialized care, the *hoofdbehandelaarschap* changes. Example: If you are a patient receiving care from an Oncologist due to coloncancer, the Oncologist will become your *hoofdbehandelaar*. After the patient received care, such as chemotherapy, and is in remission, the GP will take over *hoofdbehandelaarschap* after discharge of said patient.

*Hoofdbehandelaarschap* is crucial when it comes to caregiving within the Netherlands. This also plays a role when it comes to COVID-19 when you want to use something as 'confirmed by physician', since this can be a different physician per citizen/patient.

| Location of citizen or patient          | Hoofdbehandelaar / COVID-19 tester (if applicable)  |
| --------------------------------------- | --------------------------------------------------- |
| Citizen of The Netherlands              | General Practitioner (GP)                           |
| Admitted patient to hospital (clinical) | Medical Specialist (Oncologist, Cardiologist, etc.) |
| Patient in nursing home                 | *Verpleeghuisarts* or GP                            |
| Caregiver related to COVID-19           | Municipal Health Services (GGD)                     |

**General information COVID-19**

Incubation time: approximately 2 - 14 days (average of 5-6 days)

Infection routes: Not completely sure yet. However, this is probably through *druppels* and direct contact, such as infected areas or inhalation of *aerosolen* during *areosolvormende procedures* (example: intubation on an Intensive Care Unit). 

Infection period: no exact numbers available yet. Patient can transmit the disease during the symptomatic period of COVID-19, meaning when the patient is still showing symptoms. This is speculated to be the most common cause of spreading COVID-19. 
Virus is still found in feces and in throat after the symptomatic phase through PCR method.

Reference: [NHG: Incubatietijd, besmettingsweg en besmettelijke periode](https://corona.nhg.org/achtergronden/#incubatietijd)

**Summary workflow GP in relation to COVID-19**

When a citizen is reporting symptoms of COVID-19 (cough, fever, Shortness of Breath - SOB) he or she needs to contact their GP (phone). The GP will check if the citizen needs testing for COVID-19. 

The following criteria is used to see if a citizen needs to be tested:

- Citizen/patient has high care needs (example: chronically ill);

- Citizen/patient lives in a facility, such as nursing home, psychiatric facility, hospice.

  *Reference: [GP testing policy (Dutch)](https://www.nhg.org/sites/default/files/content/nhg_org/uploads/20200402_testbeleid_huisartsenpraktijk.pdf)*

The GP is responsible for testing the patients when they fit in any of the given criteria above. The NHG and SpoedHAG (kaderhuisartsen spoedzorg) have a triage flowchart to be used during screening of possible suspected COVID-19 patients:

![](images/A3PosterTriageSpoedHAG.png)

Image translates to:
Step 1: ask patient if they have COVID-19?
Step 2: [ABCDE-triage (Airway, Breathing, Circulation, Disabilities, Exposure/Environment)](https://www.nhg.org/themas/artikelen/herziene-abcde-kaart-voor-huisartsen-onderbouwing-en-implementatie)
Step 3: Description condition of patient
Step 4: addition medical questions about condition of patient --> *alarmsymptomen aanwezig?*
Step 5a: support patient with information about infection

However, when the citizen is a caregiver him- or herself, they need to contact the GGD to get tested. This will not be done by the GP. 

**Identification caregivers**

The GP uses UZI passes for identification. 

<!--Wat kunt u met de UZI-pas

Met de UZI-pas kunt u via de elektronische weg veilig en betrouwbaar medische gegevens uitwisselen. U kunt het burgerservicenummer (BSN) van opgenomen patiënten vastleggen en opvragen. Dit werkt via de BSN-diensten van de Sectorale Berichten Voorziening in de Zorg (SBV-Z). Ook ondersteunen deze diensten zorgaanbieders bij het vaststellen van de identificatie van de patiënt. De UZI-pas geeft als identificatie- en authenticatiemiddel toegang tot deze diensten.

Het UZI-register geeft geen advies over welke UZI-pas, in welke situatie, bij welke toepassing nodig is. Tevens is het UZI-register niet bekend met alle mogelijke applicaties waar haar middelen gebruikt worden. Twijfelt u of het mogelijk is om de UZI-pas met uw systeem te gebruiken, neem dan contact op met uw ICT-leverancier.-->

[UZI pas: wat kunt u er mee.](https://www.uziregister.nl/uzi-pas/wat-kunt-u-met-de-pas)

