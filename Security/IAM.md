# Identity and Access Management (IAM)

This document describes the Identify and Access Management approach of the project. Given the nature of the project where we encourage and appreciate anyone in the community to participate we have to outline some core principles around identify and access management of the different platforms involved for development and deployment of the project.

>The initial version of this document is focusing on the IAM roles and configurations of GitLab. We are working to include the other platforms.

## IAM Security Principles

To prevent an attacker from making unwanted changes on someone else his behalf we ask everyone to adhere to the following principles:

* **Use a unique password.** Prevent password re-usage for different platforms, use a unique platform and a password manager to securely store your passwords and/or make use of a pass-phrase (a series of random words or a sentence).
* **Enabled Two-Factor Authentication (2FA).** 2FA provides an additional level of securing your user account. Configure 2FA for each online platform involved in your development, such as GitLab.
* **Sign your commits.** Signing your commits to let everyone know that the commits is coming from a [trusted and verified source](https://help.github.com/en/github/authenticating-to-github/managing-commit-signature-verification). This helps in preventing [git spoofing](https://cybermundus.com/post/git-spoofing/).

## GitLab

### GitLab Roles

We follow the "principle of least privileges" where we limited everyone's access rights to the least necessary for its legitimate purpose. This principle is applicable to the different platforms involved as described below. The role assigned to users in the PrivateTracer group should be as minimum possible ("reporter") as this will be the default role for every underlying Project. Depending on the nature of the work, a different role ("developer") should be assigned to the project that is required.

Following [the handbook of permissions in GitLab](https://about.gitlab.com/handbook/product/#permissions-in-gitlab) and the description of the different [permissions levels](https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions), this comes down to the definition of roles as applied to for PrivateTracer.

* **Guests:** Since the project is open source and the guest account is limited to read-only access rights, its usage is limited.
* **Reporters:** Read-only contributors who can not write to the repository but are able to create and modify issues. This should be the default role assigned to contributors on a group level.
* **Developers:** Developers are direct contributors with access to the repository unless something has been explicitly restricted. This should be the default role assigned to contributors on a project level.
* **Maintainers:** Developers with unrestricted access to the repository being able to push directly to the master branch. This role should be limited to the owner of the respective Value Stream.
* **Owners:** "Group Admins". Unrestricted access to the entire project or group, being able to delete the project(s) and issues. Projects are limited to have 1 owner in addition to the owner of the group. A group can have multiple owners. This role should be limited to members of the board.

### GitLab Configuration

* **Do not commit directly to the master branch.** Changes to the master branch should be performed by means of a merge requests. Master branches should be set up as a protected branch to prevent force pushing to the branch.
* **Merge requests should be approved before they can be merged.** Since a merge to the master branch means a change to production, the merge requests should be approved by the project maintainer or owner when running in a live environment. This can be enforced by configuring [Merge Request Approvals in GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html).
* **Approval of merge requests by their committers should be prevented.** Enforce the Four Eyes Principle on merge requests. Committers [should not be able to approve their own merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#prevent-approval-of-merge-requests-by-their-committers).
* **Configure the repositories for signed-commits only**. Enforce everyone to sign their commits when pushing a change to the repository.
